rm -rf /opt/ANDRAX/lulzbuster

make lulzbuster

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin src/ inc/

cp -Rf $(pwd) /opt/ANDRAX/lulzbuster

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
